############################################################
# Dockerfile that builds a Steam Gameserver
############################################################
FROM cm2network/steamcmd:root

ENV STEAM_APPS_DIR ${HOMEDIR}/steamapps
ENV STEAM_APP_ID="376030"
ENV STEAM_APP_NAME="ark"

COPY run-server.sh ${HOMEDIR}/run-server.sh
COPY update-steam-app.sh ${HOMEDIR}/update-steam-app.sh
COPY test.sh ${HOMEDIR}/test.sh


RUN set -x \
	&& chmod 755 "${HOMEDIR}/run-server.sh"\
	&& chmod 755 "${HOMEDIR}/update-steam-app.sh"\
	&& chmod 755 "${HOMEDIR}/test.sh"\
	&& chown -cR "${USER}:${USER}" "${HOMEDIR}"

USER ${USER}

WORKDIR ${HOMEDIR}

# CMD ["bash", "entry.sh"]

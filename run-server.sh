#!/bin/bash

STEAM_APPS_DIR=${STEAM_APPS_DIR:=${HOMEDIR}/steamapps}
STEAM_APP_ID=${STEAM_APP_ID:=376030}
STEAM_APP_NAME=${STEAM_APP_NAME:=ark}
STEAM_APP_INSTANCE=${STEAM_APP_INSTANCE:=1}

export APP_DIR="${STEAM_APPS_DIR}/${STEAM_APP_ID}_${STEAM_APP_NAME}"
export APP_SERVER_DIR="${APP_DIR}/server"
export IS_STEAM_APP_INSTALLED=${APP_DIR}/installed

CNT=1
while [ ! -f  ${IS_STEAM_APP_INSTALLED} ]
do
    echo "wait for install Steam App ${STEAM_APP_ID}"
    sleep 5

    ((CNT++))
     if [ $CNT -ge 5 ];
    then
        exit 0
    fi
done

export SERVER_MAP_NAME=${SERVER_MAP_NAME:=TheIsland}
export SERVER_SESSION_NAME=${SERVER_SESSION_NAME:=steamdocker}
export SERVER_ADMINPW=${SERVER_ADMINPW:=tsd782321}
export SERVER_PORT=${SERVER_PORT:=7777}
export SERVER_QUERY_PORT=${SERVER_QUERY_PORT:=27015}

START_SERVER_SH_NAME="${APP_DIR}/run_${STEAM_APP_NAME}${STEAM_APP_INSTANCE}_${SERVER_MAP_NAME}.sh"

# mkdir -p ${APP_DIR}
# mkdir -p ${SERVER_DIR}

if [ ! -f  ${START_SERVER_SH_NAME} ]
then
    {
        echo "#!/bin/bash"
        echo ""
        echo ""
        echo "echo \"!!! start game server !!!\""
        echo "echo \"${START_SERVER_SH_NAME}\""
        echo ""
        echo ""
        echo "START_COMMAND=\"echo \\\"Server not start! Edit ${START_SERVER_SH_NAME} and put the StartCommand in it!\\\"\""
        echo ""
        echo "# Example for ARK-Server"
        echo "#ARK_SERVER_DIR=\${APP_SERVER_DIR}"
        echo "#ARK_SERVER_MAP=\${SERVER_MAP_NAME}"
        echo "#ARK_SERVER_SESSION_NAME=\${SERVER_SESSION_NAME}"
        echo "#ARK_SERVER_ADMINPW=\${SERVER_ADMINPW}"
        echo "#ARK_SERVER_PORT=\${SERVER_PORT}"
        echo "#ARK_SERVER_QUERY_PORT=\${SERVER_QUERY_PORT}"
        echo "#ARK_SERVER_MAX_PLAYER=70"
        echo "#ARK_THIRD_PERSON_PERSPECTIVE=true"
        echo "#ARK_NOTIFY_PLAYER_LEFT=true"
        echo "#ARK_NOTIFY_PLAYER_JOIN=true"
        echo "#ARK_PVE_ONLY=true"
        echo "#"
        echo "#START_COMMAND=\"\${ARK_SERVER_DIR}/ShooterGame/Binaries/Linux/ShooterGameServer \${ARK_SERVER_MAP:=TheIsland}?listen?SessionName=\${ARK_SERVER_SESSION_NAME:=my-ARK-Server}?ServerAdminPassword=\${ARK_SERVER_ADMINPW:=123admin456}?Port=\${ARK_SERVER_PORT:=7777}?QueryPort=\${ARK_SERVER_QUERY_PORT:=27015}?allowThirdPersonPlayer=\${ARK_THIRD_PERSON_PERSPECTIVE:=true}?alwaysNotifyPlayerLeft=\${ARK_NOTIFY_PLAYER_LEFT:=true}?alwaysNotifyPlayerJoined=\${ARK_NOTIFY_PLAYER_JOIN:=true}?serverPVE=\${ARK_PVE_ONLY:=true}?MaxPlayers=\${ARK_SERVER_MAX_PLAYER:=70} -nosteamclient -game -server -log\""
        echo "#"
        echo "#echo \"ARK_SERVER_DIR=\${ARK_SERVER_DIR}\""
        echo "#echo \"ARK_SERVER_MAP=\${ARK_SERVER_MAP}\""
        echo "#echo \"ARK_SERVER_SESSION_NAME=\${ARK_SERVER_SESSION_NAME}\""
        echo "#echo \"ARK_SERVER_ADMINPW=\${ARK_SERVER_ADMINPW}\""
        echo "#echo \"ARK_SERVER_PORT=\${ARK_SERVER_PORT}\""
        echo "#echo \"ARK_SERVER_QUERY_PORT=\${ARK_SERVER_QUERY_PORT}\""
        echo "#echo \"ARK_SERVER_MAX_PLAYER=\${ARK_SERVER_MAX_PLAYER}\""
        echo "#echo \"ARK_THIRD_PERSON_PERSPECTIVE=\${ARK_THIRD_PERSON_PERSPECTIVE}\""
        echo "#echo \"ARK_NOTIFY_PLAYER_LEFT=\${ARK_NOTIFY_PLAYER_LEFT}\""
        echo "#echo \"ARK_NOTIFY_PLAYER_JOIN=\${ARK_NOTIFY_PLAYER_JOIN}\""
        echo "#echo \"ARK_PVE_ONLY=\${ARK_PVE_ONLY}\""
        echo "#"
        echo "#echo \"\""
        echo "#echo \"\${START_COMMAND}\""
        echo "#"
        echo ""
        echo ""
        echo "echo \"${START_COMMAND}\""
        echo "\${START_COMMAND}"
   } >  ${START_SERVER_SH_NAME}

   chmod 755 -v ${START_SERVER_SH_NAME}
fi

bash ${START_SERVER_SH_NAME}

#!/bin/bash

STEAM_APPS_DIR=${STEAM_APPS_DIR:=${HOMEDIR}/steamapps}
STEAM_APP_ID=${STEAM_APP_ID:=376030}
STEAM_APP_NAME=${STEAM_APP_NAME:=ark}
STEAM_APP_INSTANCE=${STEAM_APP_INSTANCE:=1}

export APP_DIR="${STEAM_APPS_DIR}/${STEAM_APP_ID}_${STEAM_APP_NAME}"
export APP_SERVER_DIR="${APP_DIR}/server"
export IS_STEAM_APP_INSTALLED=${APP_DIR}/installed

export SERVER_MAP_NAME=${SERVER_MAP_NAME:=TheIsland}
export SERVER_SESSION_NAME=${SERVER_SESSION_NAME:=steamdocker}
export SERVER_ADMINPW=${SERVER_ADMINPW:=tsd782321}
export SERVER_PORT=${SERVER_PORT:=7777}
export SERVER_QUERY_PORT=${SERVER_QUERY_PORT:=27015}

echo " Test Phase 1"
echo "STEAM_APPS_DIR=${STEAM_APPS_DIR}"
echo "STEAM_APP_ID=${STEAM_APP_ID}"
echo "STEAM_APP_NAME=${STEAM_APP_NAME}"
echo "STEAM_APP_INSTANCE=${STEAM_APP_INSTANCE}"
echo ""
echo "SERVER_MAP_NAME=${SERVER_MAP_NAME}"
echo "SERVER_SESSION_NAME=${SERVER_SESSION_NAME}"
echo "SERVER_ADMINPW=${SERVER_ADMINPW}"
echo "SEREVR_PORT=${SERVER_PORT}"
echo "SERVER_QUERY_PORT=${SERVER_QUERY_PORT}"
echo ""
echo "APP_DIR=${APP_DIR}"
echo "APP_SERVER_DIR=${APP_SERVER_DIR}"
echo ""


TEST_PHASE_2_SH=${APP_DIR}/test2.sh

{
    echo "#!/bin/bash"
    echo ""
    echo ""
    echo "echo \"\""
    echo "echo \"!!! Test Phase 2 !!!\""
    echo "echo \"${TEST_PHASE_2_SH}\""
    echo "echo \"\""
    echo "echo \"STEAM_APPS_DIR=\${STEAM_APPS_DIR}\""
    echo "echo \"STEAM_APP_ID=\${STEAM_APP_ID}\""
    echo "echo \"STEAM_APP_NAME=\${STEAM_APP_NAME}\""
    echo "echo \"STEAM_APP_INSTANCE=\${STEAM_APP_INSTANCE}\""
    echo "echo \"APP_DIR=\${APP_DIR}\""
    echo "echo \"APP_SERVER_DIR=\${APP_SERVER_DIR}\""
    echo "echo \"IS_STEAM_APP_INSTALLED=\${IS_STEAM_APP_INSTALLED}\""
    echo "echo \"SERVER_MAP_NAME=\${SERVER_MAP_NAME}\""
    echo "echo \"SERVER_SESSION_NAME=\${SERVER_SESSION_NAME}\""
    echo "echo \"SERVER_ADMINPW=\${SERVER_ADMINPW}\""
    echo "echo \"SEREVR_PORT=\${SEREVR_PORT}\""
    echo "echo \"SERVER_QUERY_PORT=\${SERVER_QUERY_PORT}\""
    echo "echo \"\""
    echo ""
    echo "CNT=1"
    echo "while [ ! -f  \${IS_STEAM_APP_INSTALLED} ]"
    echo "do"
    echo "    echo \"wait for install Steam App \${STEAM_APP_ID}\""
    echo "    sleep 5"
    echo "    echo \"\""
    echo "    ((CNT++))"
    echo "     if [ \$CNT -ge 20 ];"
    echo "    then"
    echo "        echo \"Timeout! App not installed!\""
    echo "        exit 1"
    echo "    fi"
    echo "done"
    echo "echo \"\""
    echo "echo \"Steam App \${STEAM_APP_ID} successfull installed!\""
    echo "exit 0"
    echo ""
    echo ""
} >  ${TEST_PHASE_2_SH}

chmod 755 -v ${TEST_PHASE_2_SH}
bash ${TEST_PHASE_2_SH}

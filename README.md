# dockerSteam
Docker-Image für einen Steam GameServer

Das Image ist darauf ausgelegt, das der Server einmal installiert wird und daraus mehrere Instanzen gestartet werden können. Vorausgesetzt man hat genügend Arbeitsspeicher. Auf meinem Testsystem mit 10 GB Ram reicht der Speicher gerade für eine ARK-Instance (benötigt 6 bis 8GB).

Für die Installation sowie für jede Serverinstanze muss ein Container erstellt werden. Siehe dazu das Beispiel [docker-compose.yml](https://gitlab.com/Puetgeluet/arkdocker/-/blob/master/docker-compose.yml)

Folgende Pfade werden aus dem Dockercontainer in ein Volume auf dem Hostsystem gemappt
1. /var/lib/docker-compose/steam/Steam:/home/steam/Steam   (Verzeichnis für Steam)
2. /var/lib/docker-compose/steam/steamapps:/home/steam/steamapps (Verzeichnis in dem die SteamApp (GameServer Files) gespeichert werden)
3. /var/lib/docker-compose/steam/savegames/ARK1-TheIsland:/home/steam/steamapps/376030_ARK/server/ShooterGame/Saved (Verzeichnis, in dem eine Serverinstance die Spielstände speichert. Für jede Serverinstance ist auf dem Hostsystem ein eigenes Verzeichnis anzulegen.)

Die Verzeichnisse im Hostsystem (/var/lib/docker-compose/...) können natürlich nach belieben angepasst werden.
Der Dockercontainer benötigt in diesen Volumes Schreibrechte für den Standartbenutzer! Da der Dockerdämon als root ausgeführt wird, würden die Verzeichnisse dem Benutzer root gehören. Daher ist es ratsam, die Verzeichnisse auf dem Hostsystem vor dem ersten Start des Dockercontaines selbst zu erstellen. Sollen sie in einem Verzeichnis liegen, in welchem der Standartbenutzer keine Schreibrechte hat, so kann der Owner anschließend geändert werden:

    chmod -v 755 path/to/my/dir
    chown -cR user:user path/to/my/dir

Docker option für das verlinken eines Volumes:

    -v path/to/my/dir:/home/steam/steamapps

In dem Container befinden sich zwei .sh Skripte, welche beim starten des Containers als entrypoint gesetzt werden können.

    'update-steam-app.sh'
    installiert und updatet die SteamApp (GameServer)

    'run-server.sh'
    startet den GameServer. Nach dem 1. Start liegt im SteamApp-Verzeichnis eine weitere .sh Datei 'run_ARK1_TheIsland.sh'. Der Name der Datei hängt von den Umgebungsvariablen ab. In dieser Datei muss der Startbefehl für die jeweilige SteamApp eingetragen werden. Für ARK muss der vorhandene Befehl nur auskommentiert werden.




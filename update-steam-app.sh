#!/bin/bash

STEAM_APPS_DIR=${STEAM_APPS_DIR:=${HOMEDIR}/steamapps}
STEAM_APP_ID=${STEAM_APP_ID:=376030}
STEAM_APP_NAME=${STEAM_APP_NAME:=ark}

APP_DIR="${STEAM_APPS_DIR}/${STEAM_APP_ID}_${STEAM_APP_NAME}"
APP_SERVER_DIR="${APP_DIR}/server"
IS_STEAM_APP_INSTALLED=${APP_DIR}/installed

rm -f ${IS_STEAM_APP_INSTALLED}

bash ${STEAMCMDDIR}/steamcmd.sh +login anonymous \
				+force_install_dir ${APP_SERVER_DIR} \
				+app_update ${STEAM_APP_ID} \
				+quit

{
    echo "Steam App ${STEAM_APP_ID} installed."
} >  ${IS_STEAM_APP_INSTALLED}

date >>  ${APP_DIR}/updatehistory.txt
